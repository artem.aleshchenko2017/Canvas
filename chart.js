
// PIPE CHART

let fruits = {
	'apple' : 20,
	'banana' : 30,
	'orange' : 50
}

let myCanvas = document.getElementById("myCanvas");
myCanvas.width = 600;
myCanvas.height = 600;
 
let ctx = myCanvas.getContext("2d");

function drawLine(ctx, startX, startY, endX, endY){
	ctx.beginPath();
	ctx.moveTo(startX, startY);
	ctx.lineTo(endX, endY);
	ctx.stroke;
}

function drawArc(ctx, centerX, centerY, radius, startAngle, endAngle){
	// startAngle: угол начала в радианах, где начинается часть круга
	// endAngle: конечный угол в радианах, где заканчивается часть круга

	ctx.beginPath();
	ctx.arc(centerX, centerY, radius, startAngle, endAngle);
	ctx.stroke;
}

function drawPieSlice(ctx, centerX, centerY, radius, startAngle, endAngle, color ){
	ctx.fillStyle = color;
	ctx.beginPath();
	ctx.moveTo(centerX, centerY);
	ctx.arc(centerX, centerY, radius, startAngle, endAngle);
	ctx.closePath();
	ctx.fill();
}

drawLine(ctx,100,100,200,200);
drawArc(ctx, 150,150,150, 0, Math.PI/3);
drawPieSlice(ctx, 150,150,150, Math.PI/3, Math.PI/2 + Math.PI/4, '#ff0000');




let Piechart = function(options){
    this.options = options;
    this.canvas = options.canvas;
    this.ctx = this.canvas.getContext("2d");
    this.colors = options.colors;
 
    this.draw = function(){
        let total_value = 0;
        let color_index = 0;
        for (let categ in this.options.data){
            let val = this.options.data[categ];
            total_value += val;
        }
 
        let start_angle = 0;
        for (categ in this.options.data){
            val = this.options.data[categ];
            let slice_angle = 2 * Math.PI * val / total_value;
 
            drawPieSlice(
                this.ctx,
                this.canvas.width/2,
                this.canvas.height/2,
                Math.min(this.canvas.width/2,this.canvas.height/2),
                start_angle,
                start_angle+slice_angle,
                this.colors[color_index%this.colors.length]
            );
 
            start_angle += slice_angle;
            color_index++;
        }
 

        start_angle = 0;
		for (categ in this.options.data){
		    val = this.options.data[categ];
		    slice_angle = 2 * Math.PI * val / total_value;
		    let pieRadius = Math.min(this.canvas.width/2,this.canvas.height/2);
		    let labelX = this.canvas.width/2 + (pieRadius / 2) * Math.cos(start_angle + slice_angle/2);
		    let labelY = this.canvas.height/2 + (pieRadius / 2) * Math.sin(start_angle + slice_angle/2);
		 
		    if (this.options.doughnutHoleSize){
		        let offset = (pieRadius * this.options.doughnutHoleSize ) / 2;
		        labelX = this.canvas.width/2 + (offset + pieRadius / 2) * Math.cos(start_angle + slice_angle/2);
		        labelY = this.canvas.height/2 + (offset + pieRadius / 2) * Math.sin(start_angle + slice_angle/2);               
		    }
		 
		    let labelText = Math.round(100 * val / total_value);
		    this.ctx.fillStyle = "#000";
		    this.ctx.font = "bold 20px Arial";
		    this.ctx.fillText(labelText+"%", labelX,labelY);
		    start_angle += slice_angle;
		}

 
    }   
    // legend
       if (this.options.legend){
            color_index = 0;
            let legendHTML = "";

            for (categ in this.options.data){
                legendHTML += "<div><span style='display:inline-block;width:30px;height:30px;margin-bottom:5px;border:1px solid #000;background-color:"
                +this.colors[color_index++]+";'>&nbsp;</span> "+ categ + "</div>";
            }
            this.options.legend.innerHTML = legendHTML;
        }
}


let myPiechart = new Piechart(
    {
        canvas:myCanvas,
        data:fruits,
        colors:["green","yellow", "orange"],
        doughnutHoleSize:0.5,
        legend:myLegend
    }
);
myPiechart.draw();











// CLOCK   

// s = (1/60)*360 = 6°;
// m = (1/60)*6 = 0.1°;
// h = (1/60)*0.1 = 0.0017°;

function displayCanvas(){
    let canvas = document.getElementById("canvasclock");
    let context = canvas.getContext('2d');
    context.strokeRect(0, 0, canvas.width, canvas.height);

    let radiusClock = canvas.width/2 -10;
    let xCenterClock = canvas.width/2;
    let yCenterClock = canvas.height/2;


    // clear
    context.fillStyle = '#ffffff';
    context.fillRect(0,0, canvas.width, canvas.height);


    // contur and clock
    context.strokeStyle = "#000";
    context.lineWidth = 1;
    context.fillStyle = '#ccc';
    context.beginPath();
    context.arc(xCenterClock, yCenterClock, radiusClock, 0, 2*Math.PI, true);
    context.moveTo(xCenterClock, yCenterClock);
    context.stroke();
    context.fill();
    context.closePath();


    // points
    let radiusNum = radiusClock - 15;
    let radiusPoint;

    for(let tm = 0; tm < 60; tm++){

        context.beginPath();

        
        if (tm % 5 == 0) {
            radiusPoint = 7;

            
        }else{
            radiusPoint = 3;
        }

        let xPointM = xCenterClock + radiusNum * Math.cos(-6*tm*(Math.PI/180) + Math.PI/2);
        let yPointM = yCenterClock - radiusNum * Math.sin(-6*tm*(Math.PI/180) + Math.PI/2);
        context.arc(xPointM, yPointM, radiusPoint, 0, 2*Math.PI, true);
        context.fillStyle = '#A9A9A9';
        context.fill();
        context.stroke();
        context.closePath();
    }

    // 1-12 numbers
    for(let th = 1; th<=12; th++){
        context.beginPath();
        context.font = 'bold 25px tahoma';

        let xText = xCenterClock + (radiusNum -30) * Math.cos(-30*th*(Math.PI / 180) + Math.PI/2);
        let yText = yCenterClock - (radiusNum -30) * Math.sin(-30*th*(Math.PI / 180) + Math.PI/2);

        if(th<=9){
            context.strokeText(th, xText - 5, yText + 10);
        }else{
            context.strokeText(th, xText - 15, yText + 10);  
        }
    
        context.stroke();
        context.closePath();
    }

    // strelki
    let lengthSeconds = radiusNum -10;
    let lengthMinutes = radiusNum - 15;
    let lengthHour = lengthMinutes/1.5;
    let d = new Date();
    let t_sec = 6*d.getSeconds();                           
    let t_min = 6*(d.getMinutes() + (1/60)*d.getSeconds()); 
    let t_hour = 30*(d.getHours() + (1/60)*d.getMinutes());


    // sec
    context.beginPath();
    context.strokeStyle =  "#FF0000";
    context.lineWidth = 2;
    context.moveTo(xCenterClock, yCenterClock);
    context.lineTo(xCenterClock + lengthSeconds*Math.cos(Math.PI/2 - t_sec*(Math.PI/180)),
                yCenterClock - lengthSeconds*Math.sin(Math.PI/2 - t_sec*(Math.PI/180)));
    context.stroke();
    context.closePath();


    // min
    context.beginPath();
    context.strokeStyle = "#000";
    context.lineWidth = 4;
    context.moveTo(xCenterClock, yCenterClock);
    context.lineTo(xCenterClock + lengthMinutes*Math.cos(Math.PI/2 - t_min*(Math.PI/180)),
                 yCenterClock - lengthMinutes*Math.sin(Math.PI/2 - t_min*(Math.PI/180)));
    context.stroke();
    context.closePath();


    // hout
    context.beginPath();
    context.lineWidth = 7;
    context.moveTo(xCenterClock, yCenterClock);
    context.lineTo(xCenterClock + lengthHour*Math.cos(Math.PI/2 - t_hour*(Math.PI/180)),
                 yCenterClock - lengthHour*Math.sin(Math.PI/2 - t_hour*(Math.PI/180)));
    context.stroke();
    context.closePath();


    // center clock
    context.beginPath();
    context.strokeStyle = '#000';
    context.fillStyle = '#000';
    context.arc(xCenterClock, yCenterClock, 12, 0, 2*Math.PI, true);
    context.fill();
    context.stroke();
    context.closePath();


    return;
} 





window.onload = function(){
   window.setInterval(
    function(){
        let d = new Date();
        document.getElementById("clock").innerHTML = d.toLocaleTimeString();
        displayCanvas();
    }
  , 1000);
}














